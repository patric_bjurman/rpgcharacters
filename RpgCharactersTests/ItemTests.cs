﻿using System;
using Xunit;
using RpgCharacters.Character;
using RpgCharacters.Items;
using RpgCharacters.Exceptions;

namespace RpgCharactersTests
{
    public class ItemTests
    {
        [Fact]
        public void Equip_TryToEquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Weapon axe = new()
            {
                ItemName = "The Greataxe of UnitTest",
                ItemLevel = 2,
                ItemSlot = ItemSlot.WEAPON,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };
            // ACT & ASSERT
            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(axe));
        }

        [Fact]
        public void Equip_TryToEquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Armor chest = new()
            {
                ItemName = "Glittering Chestpiece of UnitTest",
                ItemLevel = 2,
                ItemSlot = ItemSlot.BODY,
                ArmorType = ArmorType.PLATE,
                Attributes = new() { Strength = 4, Vitality = 6}
                
            };
            // ACT & ASSERT
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(chest));
        }

        [Fact]
        public void Equip_TryToEquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Weapon axe = new()
            {
                ItemName = "The Rapid Bow of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.WEAPON,
                WeaponType = WeaponType.BOW,
                WeaponAttributes = new() { Damage = 12, AttackSpeed = 0.8 }
            };
            // ACT & ASSERT
            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(axe));
        }

        [Fact]
        public void Equip_TryToEquipWrongArmorType_ShouldThrowInvalidArmorException()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Armor chest = new()
            {
                ItemName = "Soft Chestpiece of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.BODY,
                ArmorType = ArmorType.CLOTH,
                Attributes = new() { Intelligence = 10, Vitality = 3 }

            };
            // ACT & ASSERT
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(chest));
        }

        [Fact]
        public void Equip_TryToEquipValidWeaponType_ShouldReturnString()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Weapon axe = new()
            {
                ItemName = "The Greataxe of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.WEAPON,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };
            // ACT
            string expectedMessage = "New Weapon equipped!";
            string actualMessage = warrior.Equip(axe);
            // ASSERT
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Equip_TryToEquipValidArmorType_ShouldReturnString()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Armor chest = new()
            {
                ItemName = "Dank Chestpiece of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.BODY,
                ArmorType = ArmorType.PLATE,
                Attributes = new() { Strength = 4, Vitality = 6 }

            };
            // ACT
            string expectedMessage = "New Armor equipped!";
            string actualMessage = warrior.Equip(chest);
            // ASSERT
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Damage_CalculateDPSWithoutWeapon_ShouldBeACertainNumber()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            // ACT
            warrior.Damage();
            double expectedDps = 1 * (1 + (5 / 100));
            double actualDps = warrior.GetCharacterDps();
            // ASSERT
            Assert.Equal(expectedDps, actualDps);
        }

        [Fact]
        public void Damage_CalculateDPSWithWeapon_ShouldBeACertainNumber()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Weapon axe = new()
            {
                ItemName = "The Greataxe of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.WEAPON,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };
            // ACT
            warrior.Equip(axe);
            warrior.Damage();
            double expectedDps = (7 * 1.1) * (1 + (5 / 100));
            double actualDps = warrior.GetCharacterDps();
            // ASSERT
            Assert.Equal(expectedDps, actualDps);
        }

        [Fact]
        public void Damage_CalculateDPSWithWeaponAndArmor_ShouldBeACertainNumber()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            Weapon axe = new()
            {
                ItemName = "The Greataxe of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.WEAPON,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor chest = new()
            {
                ItemName = "Dank Chestpiece of UnitTest",
                ItemLevel = 1,
                ItemSlot = ItemSlot.BODY,
                ArmorType = ArmorType.PLATE,
                Attributes = new() { Strength = 4, Vitality = 6 }

            };
            // ACT
            warrior.Equip(axe);
            warrior.Equip(chest);
            warrior.Damage();
            double expectedDps = (7 * 1.1) * (1 + ((5 + 1) / 100));
            double actualDps = warrior.GetCharacterDps();
            // ASSERT
            Assert.Equal(expectedDps, actualDps);
        }
    }
}
