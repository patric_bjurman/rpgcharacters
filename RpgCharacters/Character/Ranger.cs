﻿using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Character
{
    public class Ranger : Character
    {
        public Ranger(string name) : base(name)
        {
            SetUpCharacter(1, 7, 1, 8, new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL }, new WeaponType[] { WeaponType.BOW });
        }

        public override void LevelUp(int level)
        {
            if (level <= 0)
                throw new ArgumentException(string.Format("{0} is too low!", level), nameof(level));
            else
            {
                UpdateCharacter(level, 1, 5, 1, 2);
            }
        }

        public override void Damage()
        {
            UpdateDamage(TotalPrimaryAttributes.Dexterity);
        }
    }
}
