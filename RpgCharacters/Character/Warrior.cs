﻿using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Character
{
    public class Warrior : Character
    {
        public Warrior(string name) : base(name)
        {
            SetUpCharacter(5, 2, 1, 10, new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE }, new WeaponType[] { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD });
        }

        public override void LevelUp(int level)
        {
            if (level <= 0)
                throw new ArgumentException(string.Format("{0} is too low!", level), nameof(level));
            else
            {
                UpdateCharacter(level, 3, 2, 1, 5);
            }
        }

        public override void Damage()
        {
            UpdateDamage(TotalPrimaryAttributes.Strength);
        }
    }
}
