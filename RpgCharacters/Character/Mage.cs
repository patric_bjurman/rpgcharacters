﻿using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Character
{
    public class Mage : Character
    {
        public Mage(string name) : base(name)
        {
            SetUpCharacter(1, 1, 8, 5, new ArmorType[] { ArmorType.CLOTH }, new WeaponType[] { WeaponType.STAFF, WeaponType.WAND });
        }

        public override void LevelUp(int level)
        {
            if (level <= 0)
                throw new ArgumentException(string.Format("{0} is too low!", level), nameof(level));
            else
            {
                UpdateCharacter(level, 1, 1, 5, 3);
            }
        }

        public override void Damage()
        {
            UpdateDamage(TotalPrimaryAttributes.Intelligence);
        }
    }
}
