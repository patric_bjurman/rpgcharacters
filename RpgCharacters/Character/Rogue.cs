﻿using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Character
{
    public class Rogue : Character
    {
        public Rogue(string name) : base(name)
        {
            SetUpCharacter(2, 6, 1, 8, new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL }, new WeaponType[] { WeaponType.DAGGER, WeaponType.SWORD });
        }

        public override void LevelUp(int level)
        {
            if (level <= 0)
                throw new ArgumentException(string.Format("{0} is too low!", level), nameof(level));
            else
            {
                UpdateCharacter(level, 1, 4, 1, 3);
            }
        }

        public override void Damage()
        {
            UpdateDamage(TotalPrimaryAttributes.Dexterity);
        }
    }
}
