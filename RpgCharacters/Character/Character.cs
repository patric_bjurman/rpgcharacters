﻿using RpgCharacters.Exceptions;
using RpgCharacters.Interfaces;
using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Character
{
    public struct PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }

        public static PrimaryAttributes operator +(PrimaryAttributes a, PrimaryAttributes b)
        {
            return new PrimaryAttributes { Strength = a.Strength + b.Strength, Dexterity = a.Dexterity + b.Dexterity, Intelligence = a.Intelligence + b.Intelligence, Vitality = a.Vitality + b.Vitality };
        }
    }

    public struct SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public static SecondaryAttributes operator +(SecondaryAttributes a, SecondaryAttributes b)
        {
            return new SecondaryAttributes { Health = a.Health + b.Health, ArmorRating = a.ArmorRating + b.ArmorRating, ElementalResistance = a.ElementalResistance + b.ElementalResistance };
        }
    }

    public abstract class Character : IEquip
    {
        protected string Name { get; set; }
        private int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; }
        protected Dictionary<ItemSlot, Item> Equipment { get; set; } = new Dictionary<ItemSlot, Item>();
        protected ArmorType[] ArmorTypes { get; set; }
        protected WeaponType[] WeaponTypes { get; set; }
        protected double CharacterDPS { get; set; }

        /// <summary>
        /// Base constructor for character class
        /// </summary>
        /// <param name="name"></param>
        protected Character(string name)
        {
            Name = name;
            Level = 1;
            Damage();
        }

        #region Abstract Methods
        /// <summary>
        /// Calculates the characters DPS.
        /// </summary>
        public abstract void Damage();

        /// <summary>
        /// Tries to level up the character. Throws ArgumentException if param is too low.
        /// </summary>
        /// <param name="level"></param>
        public abstract void LevelUp(int level);
        #endregion

        #region Virtual Methods
        /// <summary>
        /// Show the stats of the character.
        /// </summary>
        public virtual void ShowStats()
        {
            Console.WriteLine("NAME: " + Name + "\n" + "LEVEL: " + Level.ToString() + "\n" + "Strength: " + TotalPrimaryAttributes.Strength.ToString() + "\n" + "Dexterity: " + TotalPrimaryAttributes.Dexterity.ToString() + "\n" + "Intelligence: " + TotalPrimaryAttributes.Intelligence.ToString() + "\n" +
                "Health: " + SecondaryAttributes.Health.ToString() + "\n" + "ArmorRating: " + SecondaryAttributes.ArmorRating.ToString() + "\n" + "ElementalResistance: " + SecondaryAttributes.ElementalResistance.ToString() + "\n" + "DPS: " + CharacterDPS.ToString());
        }

        /// <summary>
        /// Sets up the character with it's start stats and weapon and armors types.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="inte"></param>
        /// <param name="vit"></param>
        /// <param name="armorTypes"></param>
        /// <param name="weaponTypes"></param>
        protected virtual void SetUpCharacter(int str, int dex, int inte, int vit, ArmorType[] armorTypes, WeaponType[] weaponTypes)
        {
            UpdateAttributes(str, dex, inte, vit);
            ArmorTypes = armorTypes;
            WeaponTypes = weaponTypes;
        }

        /// <summary>
        /// Updates the characters level and attributes.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="inte"></param>
        /// <param name="vit"></param>
        protected virtual void UpdateCharacter(int level, int str, int dex, int inte, int vit)
        {
            Level += level;
            for (int i = 0; i < level; i++)
                UpdateAttributes(str, dex, inte, vit);
        }

        /// <summary>
        /// Updates the characters attributes.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="inte"></param>
        /// <param name="vit"></param>
        protected virtual void UpdateAttributes(int str, int dex, int inte, int vit)
        {
            var LevelUpAttributes = new PrimaryAttributes() { Strength = str, Dexterity = dex, Intelligence = inte, Vitality = vit };
            BasePrimaryAttributes += LevelUpAttributes;
            TotalPrimaryAttributes += LevelUpAttributes;
            SecondaryAttributes = new SecondaryAttributes() { Health = TotalPrimaryAttributes.Vitality * 10, ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity, ElementalResistance = TotalPrimaryAttributes.Intelligence };
        }

        /// <summary>
        /// Updates the characters DPS.
        /// </summary>
        /// <param name="param"></param>
        protected virtual void UpdateDamage(int param)
        {
            Weapon weapon = null;
            if (Equipment.ContainsKey(ItemSlot.WEAPON))
                weapon = (Weapon)Equipment[ItemSlot.WEAPON];
            if (weapon != null)
                CharacterDPS = weapon.GetWeaponDPS() * (1 + param / 100);
            else
                CharacterDPS = 1 * (1 + param / 100);
        }

        /// <summary>
        /// Returns the characters level. This method is to avoid setting the level outside of the character class without the LevelUp(int i) method.
        /// </summary>
        public virtual int GetLevel()
        {
            return Level;
        }

        /// <summary>
        /// Returns the characters DPS. This method is to avoid setting the character dps outside of the class.
        /// </summary>
        /// <returns></returns>
        public virtual double GetCharacterDps()
        {
            return CharacterDPS;
        }
        #endregion

        #region Interface Methods
        /// <summary>
        /// Equip an armor to a character. Throws InvalidArmorException if armor type is wrong.
        /// </summary>
        /// <param name="armor"></param>
        public string Equip(Armor armor)
        {
            bool wrongArmor = false;
            foreach (var arm in ArmorTypes)
            {
                if (armor.ArmorType == arm && armor.ItemLevel <= Level)
                {
                    if (Equipment.ContainsKey(armor.ItemSlot))
                        Equipment[armor.ItemSlot] = armor;
                    else
                        Equipment.Add(armor.ItemSlot, armor);
                    wrongArmor = false;
                    break;
                }
                else
                    wrongArmor = true;
            }
            if (!wrongArmor)
            {
                PrimaryAttributes ArmorAttributes = new();
                foreach (Item item in Equipment.Values)
                    if (item.GetType() == typeof(Armor))
                    {
                        var a = (Armor)item;
                        ArmorAttributes += a.Attributes;
                    }
                TotalPrimaryAttributes = BasePrimaryAttributes + ArmorAttributes;
                SecondaryAttributes = new SecondaryAttributes() { Health = TotalPrimaryAttributes.Vitality * 10, ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity, ElementalResistance = TotalPrimaryAttributes.Intelligence };
                return "New Armor equipped!";
            }
            else
                throw new InvalidArmorException();
        }

        /// <summary>
        /// Equip a weapon to a character. Throws InvalidWeaponException if weapon type is wrong.
        /// </summary>
        /// <param name="weapon"></param>
        public string Equip(Weapon weapon)
        {
            bool wrongWeapon = false;
            foreach (var wpn in WeaponTypes)
            {
                if (weapon.WeaponType == wpn && weapon.ItemLevel <= Level)
                {
                    if (Equipment.ContainsKey(ItemSlot.WEAPON))
                        Equipment[ItemSlot.WEAPON] = weapon;
                    else
                        Equipment.Add(ItemSlot.WEAPON, weapon);
                    wrongWeapon = false;
                    break;
                }
                else
                    wrongWeapon = true;
            }
            if (!wrongWeapon)
            {
                Damage();
                return "New Weapon equipped!";
            }
            else
                throw new InvalidWeaponException();
        }
    }
    #endregion
}
