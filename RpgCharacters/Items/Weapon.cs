﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Items
{
    public enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    public struct WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }

    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        /// <summary>
        /// Returns the weapons DPS.
        /// </summary>
        /// <returns></returns>
        public double GetWeaponDPS()
        {
            return (double)WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
        }
    }
}
