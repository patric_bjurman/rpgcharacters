﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Items
{
    public enum ItemSlot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }
    }
}
